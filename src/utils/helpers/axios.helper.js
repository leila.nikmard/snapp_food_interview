import axios from 'axios';

import * as config from "../config";

const axiosHelper = ({...params }) => {

  const header = {
    
  };

  const data = {
    method: 'get',
    headers: header,
    basURL:config.API_BASE_URL,
    ...params,
  };

  return axios(data);
};

export default axiosHelper;
