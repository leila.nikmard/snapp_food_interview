import {ActionTypes} from "../actionTypes";

const initialState = {
    vendorsList: [],
};

export const  vendorsReducer = (state = initialState , action) => {
    switch(action.type){
        case ActionTypes.LOAD_ALL_VENDORS:
            return {
                ...state,
                vendorsList: [...state.vendorsList, ...action.payload.finalResult]
            }
            default:
            return state
    }
}