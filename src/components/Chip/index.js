import React from "react";

import "./style.scss";

const Chip = ({name}) => {
    return(
        <div className="main-chip">
            {/* <span className="chip-icon">{icon}</span> */}
            <span className="name">{name}</span>
        </div>
    )
}

export default Chip