import React from "react";
import {Link} from "react-router-dom";
import "./style.scss";

const VendorCard = ({vendorData:{data}}) => {
    return (
        <Link to="/">
        <section className="vendor-cart-section">
            <div className="vendor-card-header">
                <div className="cover-image" style={{backgroundImage: `url(${data.backgroundImage})`}}></div>
                <div className="restaurant-logo">
                    <img src={data?.logo} width="100%" height="100%"/>
                </div>
            </div>
            <div className="description">
                <div className="title-container">
                    <h3 className="restaurant-title">{data?.title}</h3>
                    <div className="vendor-rate flex justify-content-start">
                        <span>{`(${data?.commentCount})`}</span>
                        <div className="rate flex justify-content-start items-center">
                            {/* <img/> */}
                            <span>{data?.rate || "جدید"}</span>
                        </div>
                    </div>
                </div>
                <div className="types-food flex">{data?.description?.split(",")?.join("")}</div>
                <div className='delivery flex justify-content-start'>
                    <div className="delivery-type">پیک فروشنده</div>
                    <div className="delivery-price">
                        <span>{data?.deliveryFee || "رایگان"}</span>
                        <span>تومان</span>
                    </div>
                </div>
            </div>
        </section>
    </Link>
    )
}

export default VendorCard;