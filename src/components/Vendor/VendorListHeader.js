import React from "react";
import filterIcon from "../../img/filter.svg"

import "./style.scss"

const VendorListHeader = ({openCount}) => {
    return(
        <div className="list-header flex justify-content-betweem items-center">
            <div className="counter">
                <h2>{`${openCount?.data}`}</h2>
            </div>
            <div className="filtering">
                <span>
                   <img src={filterIcon}/>
                </span>
                <span>به ترتیب...</span>
            </div>
        </div>
    )
}

export default VendorListHeader;